# README #



Omowumi Lynda Ademola
CS441 Mobile Game Design
January 24, 2016
Assignment 01

Create a hello world app/game to practice using labels and buttons.

Resources:
Sound effects from Soundbible.com
http://soundbible.com/877-Male-Laugh-Short.html
http://soundbible.com/62-Crowd-Boo.html
http://soundbible.com/1705-Click2.html

Sound Guidance
cs441-sound-ios (BitBucket)

----
commit ded8606e4a12e1704f3da02aa9e71c10869c1f37
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Jan 24 17:31:25 2017 -0500

    Added sound effects

commit 628f17a4abd5c9d26537362cfb5e57e1e57bccda
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Jan 24 15:08:14 2017 -0500

    created a readme file
    
Mac-Users-MacBook-6:p01-ademola Lynda$ git log
commit 79075a74ead9a30f4e984fda1ce6064a7d4f7812
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Jan 23 23:57:52 2017 -0500

    pun functionality

commit 2340eb7130e88ef3c05abfc98f00b29bfd85e592
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Jan 23 15:30:43 2017 -0500

    Got basic program working

commit ffd7e10dd38b78c2a4af0888d9bc26e38ac845bd
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Jan 23 15:07:12 2017 -0500

    moving forward with code

commit 8c53c19e8f8154c473583831d222e48e90590ac3
Author: Omowumi Ademola <oademol1@slic-40.pods.bu.int>
Date:   Thu Jan 19 17:45:03 2017 -0500

    tried to start

commit 8014e2ad20d237a0f53e5b6a96f92443f2f603f4
Author: Omowumi Ademola <oademol1@slic-40.pods.bu.int>
Date:   Thu Jan 19 17:12:26 2017 -0500

    Added all of the automatically generated files

commit 0ae1a8276ea78241e937c0f9985ea1f6524ae997
Author: Omowumi Ademola <oademol1@slic-40.pods.bu.int>
Date:   Thu Jan 19 17:07:25 2017 -0500

    Initial commit with contributors
----
