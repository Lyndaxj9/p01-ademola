//
//  AppDelegate.h
//  p01-ademola
//
//  Created by Omowumi Ademola on 1/19/17.
//  Copyright © 2017 Omowumi Ademola. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

