//
//  ViewController.h
//  p01-ademola
//
//  Created by Omowumi Ademola on 1/19/17.
//  Copyright © 2017 Omowumi Ademola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ViewController : UIViewController {
    UILabel *label;
    NSArray *strings;
    NSInteger count;
    
    SystemSoundID boo;
    SystemSoundID laugh;
    SystemSoundID button;
}

@property (nonatomic, strong) IBOutlet UILabel *label;

-(IBAction)changeMessage;

-(IBAction)booSound:(id)sender;
-(IBAction)laughSound:(id)sender;

@end

