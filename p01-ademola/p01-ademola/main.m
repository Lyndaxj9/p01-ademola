//
//  main.m
//  p01-ademola
//
//  Created by Omowumi Ademola on 1/19/17.
//  Copyright © 2017 Omowumi Ademola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
