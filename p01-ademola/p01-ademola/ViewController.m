//
//  ViewController.m
//  p01-ademola
//
//  Created by Omowumi Ademola on 1/19/17.
//  Copyright © 2017 Omowumi Ademola. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize label;

- (void)viewDidLoad {
    strings = [NSArray arrayWithObjects:@"I can't believe I got fired from the calendar factory. All I did was take a day off.", @"A man just assaulted me with milk, cream and butter. How dairy.", @"I'd tell you a chemistry joke but I know I wouldn't get a reaction.", @"I'm on a seafood diet.  Every time I see food, I eat it.", @"I wanna make a joke about sodium, but Na.", @"A garage sale is actually a garbage sale but the 'b' is silent.", nil];
    count = 0;
    
    NSURL *laughSound = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"malelaughshort" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)laughSound, & laugh);
    
    NSURL *booSound = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"crowdboo" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)booSound, & boo);
    
    NSURL *pressSound = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"click2" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pressSound, & button);
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)laughSound:(id)sender
{
    AudioServicesPlaySystemSound(laugh);
}

-(IBAction)booSound:(id)sender
{
    AudioServicesPlaySystemSound(boo);
}

-(IBAction)changeMessage
{
    //[label setText:@"I said hello"];
    if (count < [strings count])
    {
        [label setText:strings[count]];
        count++;
    } else
    {
        count = 0;
        [label setText:@"Hello World"];
    }
    
    AudioServicesPlaySystemSound(button);
    
}

@end
